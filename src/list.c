#define WIN32_LEAN_AND_MEAN
#include <tchar.h>
#include <windows.h>
#include <commctrl.h>

#include "processes.h"


HWND CreateListView(HWND hwndParent, HINSTANCE hInst)
{
    INITCOMMONCONTROLSEX icex;
    icex.dwSize = sizeof(icex);
    icex.dwICC = ICC_LISTVIEW_CLASSES;
    if(!InitCommonControlsEx(&icex))
        return NULL;

    RECT rcClient;
    if(!GetClientRect(hwndParent, &rcClient))
        return NULL;

    return CreateWindow(WC_LISTVIEW,
                        TEXT(""),
                        WS_VISIBLE | WS_CHILD | LVS_LIST | LVS_SINGLESEL,
                        0, 0,
                        rcClient.right - rcClient.left,
                        rcClient.bottom - rcClient.top,
                        hwndParent,
                        (HMENU)1,
                        hInst,
                        NULL);
}

BOOL InsertListViewItems(HWND hwndListView, ULONG nItems,
                         PTCHAR* aTexts, PDWORD aData)
{
    int iSelected = ListView_GetSelectionMark(hwndListView);

    ListView_DeleteAllItems(hwndListView);

    LVITEM lvI = {0};
    lvI.mask = LVIF_TEXT | LVIF_PARAM;
    for(ULONG i = 0; i < nItems; i++)
    {
        lvI.pszText = aTexts[i];
        lvI.iItem   = i;
        lvI.lParam  = aData[i];

        if(ListView_InsertItem(hwndListView, &lvI) == -1)
            return FALSE;
    }

    if(iSelected >= 0)
    {
        UINT iState = LVIS_SELECTED | LVIS_FOCUSED;
        ListView_SetItemState(hwndListView, iSelected, iState, iState);
    }

    return TRUE;
}

BOOL ListViewProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    _CRT_UNUSED(wParam);

    if(uMsg == WM_NOTIFY)
    {
        if(((LPNMHDR)lParam)->code == NM_DBLCLK)
        {
            HWND hListView = GetDlgItem(hWnd, 1);
            if (!hListView)
                return TRUE;

            LPNMITEMACTIVATE lpnmitem = (LPNMITEMACTIVATE)lParam;

            LVITEM lvI = {0};
            lvI.mask = LVIF_PARAM;
            lvI.iItem = lpnmitem->iItem;
            ListView_GetItem(hListView, &lvI);

            MessageBox(hWnd, GetProcessInfo((DWORD)lvI.lParam),
                       TEXT("Process info"), MB_OK | MB_ICONINFORMATION);
            return TRUE;
        }
    }
    return FALSE;
}
