#ifndef _PROCESSES_H_
#define _PROCESSES_H_
#define WIN32_LEAN_AND_MEAN
#include <tchar.h>
#include <windows.h>


PTCHAR GetProcessInfo(DWORD dwProcessId);
DWORD GetProcessesData(DWORD nSize, PTCHAR* aNames, PDWORD aIds);

#endif  // _PROCESSES_H_
