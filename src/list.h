#ifndef _LIST_H_
#define _LIST_H_

#define WIN32_LEAN_AND_MEAN
#include <windows.h>

HWND CreateListView(HWND hwndParent, HINSTANCE hInst);
BOOL InsertListViewItems(HWND hwndListView, int ULONG,
                         PTCHAR* aTexts, PDWORD aData);
BOOL ListViewProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

#endif  // _LIST_H_
