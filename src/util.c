#include <stdio.h>
#include <stdlib.h>
#include <tchar.h>

#include "util.h"


void ReportError(HWND hWnd, const PTCHAR format, ...)
{
    DWORD dwLastError = GetLastError();
    TCHAR* pszError = NULL;
    DWORD nErrorSize = FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER |
                                     FORMAT_MESSAGE_FROM_SYSTEM |
                                     FORMAT_MESSAGE_IGNORE_INSERTS,
                                     NULL,
                                     dwLastError,
                                     MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
                                     (LPTSTR)&pszError,
                                     0, NULL);
    if(!nErrorSize)
        abort();

    va_list args;
    va_start(args, format);
    int iMessageSize = _vsntprintf(NULL, 0, format, args);
    TCHAR* pszMessage = LocalAlloc(
        LPTR, (iMessageSize + nErrorSize + 2) * sizeof(TCHAR));
    if(!pszMessage)
        goto cleanup;

    _vsntprintf(pszMessage, iMessageSize + 1, format, args);
    va_end(args);

    pszMessage[iMessageSize] = '\n';

    _tcsncpy(pszMessage + iMessageSize + 1, pszError, nErrorSize + 1);

    MessageBox(hWnd, pszMessage, _T("Error"), MB_OK | MB_ICONERROR);

cleanup:
    LocalFree(pszMessage);
    LocalFree(pszError);
    exit(dwLastError);
}
