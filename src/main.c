#define WIN32_LEAN_AND_MEAN
#include <tchar.h>
#include <windows.h>
#include <malloc.h>

#include "list.h"
#include "processes.h"
#include "util.h"


LRESULT CALLBACK WindowProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    if(ListViewProc(hWnd, uMsg, wParam, lParam))
        return 0;

    switch(uMsg)
    {
    case WM_DESTROY:
        PostQuitMessage(0);
        return 0;

    case WM_TIMER:
    {
        HWND hListView = GetDlgItem(hWnd, 1);
        if(!hListView)
            return 0;

        DWORD nItems = GetProcessesData(0, NULL, NULL);
        if(nItems)
        {
            PTCHAR* aNames = _alloca(nItems * sizeof(PTCHAR));
            PDWORD aIds = _alloca(nItems * sizeof(DWORD));
            if(GetProcessesData(nItems, aNames, aIds))
            {
                InsertListViewItems(hListView, nItems, aNames, aIds);

                for(DWORD i = 0; i < nItems; i++)
                    LocalFree(aNames[i]);
            }
        }
        return 0;
    }
    }

    return DefWindowProc(hWnd, uMsg, wParam, lParam);
}

static const TCHAR* pszClassName = TEXT("Otus Process Explorer Window Class");

int WINAPI
WinMain(HINSTANCE hInst, HINSTANCE hPrevInst, LPSTR lpCmdLine, int nCmdShow)
{
    _CRT_UNUSED(hPrevInst);
    _CRT_UNUSED(lpCmdLine);

    WNDCLASS wc = {0};
    wc.lpfnWndProc = WindowProc;
    wc.hInstance = hInst;
    wc.lpszClassName = pszClassName;
    if(!RegisterClass(&wc))
        ReportError(NULL, TEXT("Failed to register window class."));

    HWND hWnd = CreateWindowEx(
        0,
        pszClassName,
        TEXT("Otus Process Explorer"),
        WS_OVERLAPPEDWINDOW,
        CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,
        NULL,
        NULL,
        hInst,
        NULL);
    if(!hWnd)
        ReportError(NULL, TEXT("Failed to create window."));

    HWND hListView = CreateListView(hWnd, hInst);
    if(!hListView)
        ReportError(hWnd, TEXT("Failed to create list view."));

    ShowWindow(hWnd, nCmdShow);

    if(!SetTimer(hWnd, 1, 500, NULL))
        ReportError(hWnd, TEXT("Failed to create timer."));

    MSG msg = {0};
    while(GetMessage(&msg, NULL, 0, 0))
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    return 0;
}
