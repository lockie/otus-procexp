#define PSAPI_VERSION 1
#define WIN32_LEAN_AND_MEAN
#include <stdio.h>
#include <tchar.h>
#include <windows.h>
#include <psapi.h>


DWORD GetProcessesData(DWORD nSize, PTCHAR* aNames, PDWORD aIds)
{
    if(nSize == 0)
    {
        DWORD nCurrSize = sizeof(DWORD);
        DWORD nNeeded = 0;
        DWORD* aProcesses = LocalAlloc(LPTR, nCurrSize);
        do
        {
            nCurrSize *= 2;
            aProcesses = LocalReAlloc(aProcesses, nCurrSize, LMEM_MOVEABLE);
            if(aProcesses == NULL)
                return 0;

            if(!EnumProcesses(aProcesses, nCurrSize, &nNeeded))
            {
                LocalFree(aProcesses);
                return 0;
            }
        } while(nNeeded == nCurrSize);
        LocalFree(aProcesses);
        return nNeeded / sizeof(DWORD);
    }

    DWORD nNeeded;
    if(!EnumProcesses(aIds, nSize * sizeof(DWORD), &nNeeded))
        return 0;
    DWORD nProcesses = nNeeded / sizeof(DWORD);
    for(DWORD i = 0; i < nProcesses; i++)
    {
        TCHAR szProcessName[MAX_PATH] = TEXT("<unknown>");
        HANDLE hProcess = OpenProcess(PROCESS_QUERY_INFORMATION |
                                      PROCESS_VM_READ,
                                      FALSE,
                                      aIds[i]);
        if(hProcess)
        {
            HMODULE hMod;
            DWORD cbNeeded;
            if(EnumProcessModules(hProcess, &hMod, sizeof(hMod), &cbNeeded))
            {
                GetModuleBaseName(hProcess, hMod, szProcessName, MAX_PATH);
            }
        }

        size_t nNameLength = _tcslen(szProcessName) + 1;
        aNames[i] = LocalAlloc(LPTR, nNameLength * sizeof(TCHAR));
        _tcsncpy(aNames[i], szProcessName, nNameLength);

        CloseHandle(hProcess);
    }
    return nProcesses;
}

#define BUFFER_SIZE 4096
static TCHAR szBuffer[BUFFER_SIZE];

PTCHAR GetProcessInfo(DWORD dwProcessId)
{
    HANDLE hProcess = OpenProcess(PROCESS_QUERY_INFORMATION |
                                  PROCESS_VM_READ,
                                  FALSE, dwProcessId);
    if(!hProcess)
        return NULL;

    ZeroMemory(szBuffer, BUFFER_SIZE);
    size_t nLength = 0;

    HMODULE hMods[1024];
    DWORD cbNeeded;
    if(EnumProcessModules(hProcess, hMods, sizeof(hMods), &cbNeeded))
    {
        for(DWORD i = 0; i < cbNeeded / sizeof(HMODULE); i++)
        {
            TCHAR szModName[MAX_PATH];

            DWORD nModLength = GetModuleFileNameEx(
                hProcess, hMods[i], szModName, MAX_PATH);
            if(nModLength)
            {
                nLength += _sntprintf(
                    szBuffer + nLength, BUFFER_SIZE - nLength - 1,
                    TEXT("%s [0x%p]\n"), szModName, (void*)hMods[i]);
            }
        }
    }

    CloseHandle(hProcess);
    return szBuffer;
}
