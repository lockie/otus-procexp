#ifndef _UTIL_H_
#define _UTIL_H_

#define WIN32_LEAN_AND_MEAN
#include <windows.h>


DECLSPEC_NORETURN void ReportError(HWND hWnd, const PTCHAR format, ...);

#endif  // _UTIL_H_
